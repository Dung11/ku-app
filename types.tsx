export type RootStackParamList = {
  Root: undefined;
  NotFound: undefined;
  Author: undefined;
  Login: undefined;
  Register: undefined;
  Xsmb: undefined;
  Xsmn: undefined;
  Xsmt: undefined;
};

export type BottomTabParamList = {
  TabOne: undefined;
  TabTwo: undefined;
  TabThree: undefined;
};

export type TabOneParamList = {
  TabOneScreen: undefined;
};

export type TabTwoParamList = {
  TabTwoScreen: undefined;
};
