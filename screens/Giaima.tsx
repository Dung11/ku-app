import * as React from 'react';
import { Dimensions, FlatList, StyleSheet } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';

import { Text, View } from '../components/Themed';
const screen = Dimensions.get('window')

const DATA = [
    {
        id: 1,
        number: '10 - 01',
        name: "Anh em gặp nhau"
    },
    {
        id: 2,
        number: '38 - 93 - 54',
        name: "Ao hồ sông ngòi"
    },
    {
        id: 3,
        number: '25 - 52',
        name: "Ao ước"
    },
    {
        id: 4,
        number: '76',
        name: "Ba ba"
    },
    {
        id: 5,
        number: '19',
        name: "Ba bố con ăn no"
    }



]

export default function GiaiMaScreen() {
    const [loading, setLoading] = React.useState(true);
    const renderItem = ({ item, index }) => {

        return (
            <View style={[styles.item,{backgroundColor: index%2===0 ? '#c5eee6': 'white'}]}>
                <View style={{backgroundColor: 'transparent' , flexDirection: 'row' , alignItems: 'center', height: screen.height / 10}}>
                    <View style={styles.itemLeft}>
                        <Text>{item.id}</Text>
                    </View>
                    <Text>{item.name}</Text>
                </View>
                <View style={styles.itemLeft}>
                    <Text>{item.number}</Text>
                </View>
            </View>
        );
    };
    return (
        <View style={styles.container}>
            <TextInput style={{paddingHorizontal: 10, marginBottom: 10  ,height: screen.height/10, width: '100%',borderWidth:  1, borderColor: 'lightgray'}} placeholder="Tìm kiếm"  />
            {/* {loading ? <NotFoundScreen /> : null} */}
            <FlatList
                data={DATA}
                renderItem={renderItem}
            />

        </View>
    );
}

var styles = StyleSheet.create({
    container: {
        flex: 1,
    }
    , 
    item: {
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
        height: screen.height / 14,
        width: '100%',
        borderWidth: 1,
        borderColor: 'lightgray',
        marginBottom: 10,
    },
    itemLeft: {
        height: screen.height / 14,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'lightgray',
        paddingHorizontal: 5,
        marginRight: 10
    },
    itemRight: {
        height: screen.height / 14,
        width: '30%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'lightgray'
    }
});
