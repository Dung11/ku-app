import axios, { AxiosInstance } from "axios";
const baseURL = 'https://soccer.sportmonks.com/api/v2.0'
class Service {
  private instance: any;

  async getFixtures() {
    try {
      const res = await axios.get(`${baseURL}/fixtures/date/2018-11-23?api_token=ak15cu397E3y0ErWtznLA1QiQeBGLs9yVAm10LD0aVbZfHLpL3uZPIDo7H6S&include=localTeam,visitorTeam,events,league`,{
        headers: {
          "Content-Type": "application/json"
        },
      });

      return res.data;
    } catch (error) {
      console.log("e",error);
    }
  }



}

export const apiSoccer = new Service();
