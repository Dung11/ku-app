import { StackScreenProps } from '@react-navigation/stack';
import * as React from 'react';
import { Dimensions, Text, View, StyleSheet, FlatList, TouchableOpacity, ImageBackground } from 'react-native';
import WebView from 'react-native-webview';

import EditScreenInfo from '../components/EditScreenInfo';
import { RootStackParamList } from '../types';

const DATA = [
  {
    name: 'KQ xổ số Miền Bắc'
  },
  {
    name: 'KQ xổ số Miền Trung'
  },
  {
    name: 'KQ Xổ số Miền Nam'
  },
 
]

const screen = Dimensions.get('window');

export default function TabOneScreen({
  navigation,
}: StackScreenProps<RootStackParamList>) {
  const onClick = (index: any) => {
    if (index === 0) {
      navigation.navigate('Xsmb')
    }
    if (index === 1) {
      navigation.navigate('Xsmt')
    }
    if (index === 2) {
      navigation.navigate('Xsmn')
    }
  }
  const renderItem = ({ item, index }) => {

    return (
      <TouchableOpacity style={styles.item} onPress={() => { onClick(index) }}>
        <ImageBackground style={styles.item} resizeMode="contain" imageStyle={{borderRadius: screen.width / 2.5}} source={require('../assets/images/kqxs.jpg')}>
        <Text style={{ textAlign: 'center' , color: 'blue'}}>{item.name}</Text>
        </ImageBackground>
      </TouchableOpacity>
    );
  };
  return (
    <View style={styles.container}>
      <FlatList
        numColumns={2}
        columnWrapperStyle={{ alignItems: 'center', justifyContent: 'center', paddingVertical: 20}}
        data={DATA}
        renderItem={renderItem}
      />
    </View>
  );
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 16
  },
  item: {
    alignItems: 'center',
    justifyContent: 'flex-end',
    height: screen.width / 2.5,
    width: screen.width / 2.5,
    backgroundColor: 'white',
    
    margin: 10,
    borderRadius: screen.width / 2.5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    paddingBottom: 10
  }
});
