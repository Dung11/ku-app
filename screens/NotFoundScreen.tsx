import React from 'react';
import { Modal, View, StyleSheet, ActivityIndicator } from "react-native"

interface Props {
  color?: string,
  backgroundColor?: string;
  size?: number | 'small' | 'large',
}


export const NotFoundScreen = ({ color, backgroundColor, size }: Props) => {
  return (
    <Modal transparent>
      <View style={[styles.container]}>
        <View style={[styles.content]}>
          <ActivityIndicator color={color} size={size} />
        </View>
      </View>
    </Modal>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.6)'
  },
  content: {
    padding: 20,
    backgroundColor: '#FFFFFF',
    borderRadius: 10
  }
})
