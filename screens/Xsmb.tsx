import * as React from 'react';
import { Dimensions, StyleSheet } from 'react-native';
import WebView from 'react-native-webview';

import EditScreenInfo from '../components/EditScreenInfo';
import { Text, View } from '../components/Themed';
import { NotFoundScreen } from './NotFoundScreen';
const screen = Dimensions.get('window')
const url = "http://ku-apk.club/kqxs/"
const html = `<div class="vuaketqua-embed-container" style="width: 100%"><div class="vuaketqua-embed-widget"></div><script type="text/javascript" src="https://ssl.latcdn.com/vuaketqua/embed.js" async="" data-part="xsmb" data-compact="false"></script></div>

`
export default function XsmbScreen() {
    const [loading, setLoading] = React.useState(true);
    return (
        <View style={styles.container}>
            {loading ? <NotFoundScreen /> : null}
            <WebView scalesPageToFit={true} source={{ html: html }} style={styles.webView} javaScriptEnabled={true} onLoadEnd={() => { setLoading(false) }}/>
            <Text style={{textAlign: 'center' , color: 'blue'}}>Bạn có thể dùng tay phóng to để xem cho rõ hơn, Chúc bạn may mắn!.</Text>
        </View>
    );
}

var styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'lightgray'
    }, webView: {
        height: screen.height,
        width: '100%'
    }
});
