import * as React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    Dimensions,
    Image,
    Linking,
    Alert
} from 'react-native';
import EditScreenInfo from '../components/EditScreenInfo';
import { StackScreenProps } from '@react-navigation/stack';
import { RootStackParamList } from '../types';
import { useState } from 'react';
import firebase from 'firebase';

export default function AuthorScreen({
    navigation,
}: StackScreenProps<RootStackParamList>) {
    const onClick = (index: any) => {
        if (index === 1) {
            navigation.navigate('Login')
        }
        if (index === 2) {
            navigation.navigate('Register')
        }
        if (index === 3) {
            Linking.openURL('https://chat.zalo.me/')
        }
        if (!index) {
            Alert.alert("Hệ thống đang bảo trì, vui lòng tắt ứng dụng và thử lại sau!")
        }
    }
    const onGOTO = () => {
        navigation.navigate('Root');
    }
    const [hoTro, setHoTro] = useState('');


    const [dangKy, setDangKy] = useState('');
    const [dangNhap, setDangNhap] = useState('');
    // React.useEffect(() => {
    //     try {
    //         firebase.database().ref('/').on('value', (res: any) => {
    //             setHoTro(res.val().hotro);
    //             setDangNhap(res.val().dangnhap);
    //             setDangKy(res.val().dangnhap);
    //         });
    //     } catch (error) {
    //         Alert.alert("Hệ thống đang bảo trì, vui lòng tắt ứng dụng và thử lại sau!")
    //     }
    // }, [])
    return (
        <SafeAreaView style={styles.body}>
            <StatusBar barStyle="dark-content" backgroundColor="white" />
            {/* <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={styles.scrollView}> */}
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                {/* <Image
            style={{height: 130, width: 140}}
            source={require('../assets/images/ku-app.png')}
          /> */}
                <TouchableOpacity style={styles.bnt} onPress={() => {onClick(2) }}>
                    <Text style={styles.titleBNT}>Đăng ký KU</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={[styles.bnt, { backgroundColor: 'white' }]}
                    onPress={() => { onClick(1)}}>
                    <Text style={[styles.titleBNT, { color: '#df9652' }]}>Đăng nhập KU</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={[styles.bnt, { backgroundColor: '#446084' }]}
                    onPress={() => { onGOTO() }}>
                    <Text style={[styles.titleBNT, { color: 'white' }]}>Trang chủ</Text>
                </TouchableOpacity>
          

            </View>

            <View style={{ alignItems: 'flex-end', width: '100%' }}>
                <TouchableOpacity
                    style={{
                        borderColor: '#00c0c5',
                        borderWidth: 3,
                        height: 65,
                        width: 65,
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderRadius: 40,
                        marginBottom: 10,
                       
                    }}
                    onPress={() => { onClick(3) }}>
                    <Image
                        style={{ height: 50, width: 50 }}
                        source={require('../assets/images/chat.png')}
                    />
                </TouchableOpacity>
            </View>

            {/* </ScrollView> */}
        </SafeAreaView>
    );
}

const screen = Dimensions.get('window');
const styles = StyleSheet.create({
    scrollView: {
        backgroundColor: 'white',
    },
    engine: {
        position: 'absolute',
        right: 0,
    },
    body: {
        backgroundColor: 'white',
        paddingHorizontal: 16,
        paddingVertical: 16,
        flex: 1,
        justifyContent: 'space-between'
    },
    bnt: {
        borderWidth: 1,
        borderColor: '#df9652',
        backgroundColor: '#df9652',
        width: '100%',
        height: screen.width / 7,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
        marginBottom: 10,
    },
    titleBNT: {
        color: 'white',
        fontWeight: 'bold'
    },
});
