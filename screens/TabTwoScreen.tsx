import * as React from 'react';
import { Dimensions, StyleSheet } from 'react-native';
import WebView from 'react-native-webview';

import EditScreenInfo from '../components/EditScreenInfo';
import { Text, View } from '../components/Themed';
import { apiSoccer } from './api/index'
import { NotFoundScreen } from './NotFoundScreen';
const screen = Dimensions.get('window')

export default function TabTwoScreen() {
  const url = "https://www.livescore.bz/"
  const [loading, setLoading] = React.useState(true);
  return (
    <View style={styles.container}>
           {loading ? <NotFoundScreen /> : null}
            <WebView scalesPageToFit={true} source={{ uri: url }} style={styles.webView} javaScriptEnabled={true} onLoadEnd={() => { setLoading(false) }}/>

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ff00ff'
}, webView: {
    height: screen.height,
    width: '100%'
}
});
