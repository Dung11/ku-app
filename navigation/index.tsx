import { NavigationContainer, DefaultTheme, DarkTheme } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';
import { ColorSchemeName } from 'react-native';

import { RootStackParamList } from '../types';
import BottomTabNavigator from './BottomTabNavigator';
import AuthorScreen from '../screens/Author';

import LinkingConfiguration from './LinkingConfiguration';
import Login from '../screens/Login';
import Register from '../screens/Register';
import XsmbScreen from '../screens/Xsmb';
import XsmnScreen from '../screens/Xsmn';
import XsmtScreen from '../screens/Xsmt';

// If you are not familiar with React Navigation, we recommend going through the
// "Fundamentals" guide: https://reactnavigation.org/docs/getting-started
export default function Navigation({ colorScheme }: { colorScheme: ColorSchemeName }) {
  return (
    <NavigationContainer
      linking={LinkingConfiguration}
      theme={colorScheme === 'dark' ? DarkTheme : DefaultTheme}>
      <RootNavigator />
    </NavigationContainer>
  );
}

// A root stack navigator is often used for displaying modals on top of all other content
// Read more here: https://reactnavigation.org/docs/modal
const Stack = createStackNavigator<RootStackParamList>();

function RootNavigator() {
  return (
    <Stack.Navigator initialRouteName="Author" >
      <Stack.Screen name="Author" component={AuthorScreen} options={{headerShown: false}}/>
      <Stack.Screen name="Login" component={Login} options={{headerTitle: 'Hướng dẫn đăng nhập KU'}}/>
      <Stack.Screen name="Register" component={Register} options={{headerTitle: 'Hướng dẫn đăng ký KU'}}/>
      <Stack.Screen name="Xsmb" component={XsmbScreen} options={{headerTitle: 'Xổ số Miền Bắc mới nhất'}}/>
      <Stack.Screen name="Xsmn" component={XsmnScreen} options={{headerTitle: 'Xổ số Miền Nam mới nhất'}}/>
      <Stack.Screen name="Xsmt" component={XsmtScreen} options={{headerTitle: 'Xổ số Miền Trung mới nhất'}}/>

      <Stack.Screen name="Root" component={BottomTabNavigator} options={{headerTitle: 'Trang chủ'}}/>
    </Stack.Navigator>
  );
}
